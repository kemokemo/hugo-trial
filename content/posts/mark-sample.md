---
title: "Mark Sample"
date: 2020-11-25T22:55:09+09:00
---

# 記号などの記載方法

登録商標は、markdownで`&reg;`と記載することで、&reg;というように表示することができる。
同様にトレードマークも、markdownで`&trade;`と記載することで、&trade;この通り表示可能。
