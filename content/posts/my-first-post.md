---
title: "My First Post"
date: 2019-07-06T18:50:49+09:00
---

## こんにちは, Hugo!

これは私が[Hugo](https://gohugo.io/)で作る初めての静的サイトです。

{{< highlight golang >}}
package main

import "fmt"

func main() {
	fmt.Println("Hello, Hugo!")
}
{{< / highlight >}}

[Go言語](https://golang.org/)最高です!